import Vue from 'vue'
import Router from 'vue-router'
import BabyLoader from '@/components/BabyLoader'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'BabyLoader',
      component: BabyLoader
    }
  ]
})
